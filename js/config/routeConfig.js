angular.module('listaTelefonica').config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');

    $routeProvider
    .when('/', {
        templateUrl: './view/inicio.html',
        controller: function() {

        }
    })
    .when('/contatos', {
        templateUrl: './view/contatos.html',
        controller: 'listaTelefonicaCtrl'
    })
    .when('/contatos/novo', {
        templateUrl: './view/novoContato.html',
        controller: 'novoContatoCtrl'
    })
    .otherwise({redirectTo: '/contatos'})
});
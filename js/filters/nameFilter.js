angular.module('listaTelefonica').filter('name', function() {
    return function(input) {
        var listaDeNomes = input.split(' ');

        var listaDeNomesUpper = listaDeNomes.map(function(nome) {
            if(/(de|da)/.test(nome)) return nome;
            return nome.charAt(0).toUpperCase() + nome.substring(1).toLowerCase();
        });

        console.log(listaDeNomesUpper);

        return listaDeNomesUpper.join(' ');
    };
});
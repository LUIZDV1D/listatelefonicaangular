angular.module('listaTelefonica')
            .controller('novoContatoCtrl', function ($scope, contatosAPI, operadorasAPI, serialGenerator, $location) {
                
                $scope.operadoras = [];

                var carregarOperadoras = function() {
                    operadorasAPI.getOperadoras().then(function (response) {
                        $scope.operadoras = response.data;
                    })
                }

                $scope.AdicionarContato = function(contato) {
                    contato.serial = serialGenerator.generate();
                    contato.data = new Date();
                    contatosAPI.saveContato(contato).then(function() {
                        delete $scope.contato;
                        $scope.contatoForm.$setPristine();
                        $location.path('/contatos')
                        carregarContatos();
                    });
                };


                carregarOperadoras();

            });